package niit.summer.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class UIForm extends JFrame {

	public static final int ROWS = 4;
	public static final int COLS = 3;
	
	private JPanel panel;
	private ImageButton lastButton;
	URL file;
	private ArrayList<ImageButton> buttons;
	private GameController controller = new GameController(panel);

	public UIForm()
	{
		file = UIForm.class.getClassLoader().getResource("niit/summer/ui/duke.png");
	    buttons = new ArrayList<ImageButton> (); 
		panel = new JPanel(new GridLayout(ROWS, COLS));
        lastButton = ButtonsFactory.createLastButton(11);
	}

	public void initUI() {
	        
		
		Image[][] images = ImageProcessor.cutImage(file, ROWS, COLS);
		
		int count = 0;
		
		  for (int i = 0; i < ROWS; i++) {

	            for (int j = 0; j < COLS; j++) {

	                ActionListener event = new ClickAction(panel, buttons, controller);
	                
	            	ImageButton button = ButtonsFactory.createImageButton(images[i][j], count, event);
	                count++;
 
	                if(count < 12)
	                    buttons.add(button);
	                
	            }
	        }
		    Collections.shuffle(buttons);
	        buttons.add(lastButton);
	        
	        for (int i = 0; i < 12; i++) {

	            ImageButton button = buttons.get(i);
	            panel.add(button);
	        }
	        add(panel);
	        pack();
	        panel.setBorder(BorderFactory.createLineBorder(Color.gray));
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setTitle("Puzzle");
			setVisible(true);
			setResizable(false);
	}

	public Container getPanel() {
		return panel;
	}

}
