package niit.summer.ui;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionListener;

public class ButtonsFactory {

	public static ImageButton createImageButton(Image mockImage, int position, ActionListener actionListener) {
		ImageButton button = new ImageButton(mockImage);
		button.putClientProperty("position", position);
		button.addActionListener(actionListener);
		button.setPreferredSize(new Dimension(mockImage.getWidth(null), mockImage.getHeight(null)));
		return button;
	}

	public static ImageButton createLastButton(int position) {
		ImageButton button = new ImageButton();
		button.putClientProperty("position", position);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		return button;
	}

}
