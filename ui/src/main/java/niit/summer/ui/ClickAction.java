package niit.summer.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPanel;

public class ClickAction extends AbstractAction {
	private JPanel panel;
	private List<ImageButton> buttons = new ArrayList<ImageButton> ();
	private GameController controller;

	public ClickAction(JPanel panel, List<ImageButton> buttons, GameController controller) {
		this.panel = panel;
		this.buttons = buttons;
		this.controller = controller;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		checkButton(e);
		controller.checkSolution(buttons);
		
	}

	private void checkButton(ActionEvent e) {
		 int lidx = 0;
         for (ImageButton button : buttons) {
             if (button.isLastButton()) {
                 lidx = buttons.indexOf(button);
             }
         }

         JButton button = (JButton) e.getSource();
         int bidx = buttons.indexOf(button);
         boolean ClickedButtonLastCol = (bidx%UIForm.COLS) == 2;
         boolean LastButtonLastCol = (lidx%UIForm.COLS) == 2;
         boolean ClickedButtonFirstCol = (bidx%UIForm.COLS) == 0;
         boolean LastButtonFirstCol = (lidx%UIForm.COLS) == 0;
         
         if (((Math.abs(bidx - lidx) == 1) || (Math.abs(bidx - lidx) == 3))&&!((Math.abs(bidx - lidx) == 1)&&((ClickedButtonLastCol&&LastButtonFirstCol)||(ClickedButtonFirstCol&&LastButtonLastCol)))) {
             Collections.swap(buttons, bidx, lidx);
             updateButtons();
         }
	}

	private void updateButtons() {
	
		 panel.removeAll();

         for (ImageButton btn : buttons) {

             panel.add(btn);
         }

         panel.validate();
		
	}


}
