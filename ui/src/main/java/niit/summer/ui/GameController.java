package niit.summer.ui;

import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class GameController {

	private JPanel panel;
	
	public GameController(JPanel panel) {
		this.panel = panel;
	}

	public void checkSolution(List<ImageButton> buttons) {
		
		for(ImageButton btn : buttons)
		{
			if(buttons.indexOf(btn) != (Integer) btn.getClientProperty("position"))
				return;
		}
		
		panel.removeAll();
	}

}
