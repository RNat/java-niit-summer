package niit.summer.ui;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

public class ImageButton extends JButton {
	
	private boolean isLastButton;

	public ImageButton(Image mock) {
		super(new ImageIcon(mock));
		 initButton();
		 isLastButton = false;
		 BorderFactory.createLineBorder(Color.gray);
	}


	public ImageButton() {
		super();
		initButton();
		setLastButton();
	}
	
	public void initButton()
	{

        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                setBorder(BorderFactory.createLineBorder(Color.yellow));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setBorder(BorderFactory.createLineBorder(Color.gray));
            }
        });
	}

	public boolean isLastButton() {
		return isLastButton;
	}


	public void setLastButton() {
		isLastButton = true;
		
	}

}
