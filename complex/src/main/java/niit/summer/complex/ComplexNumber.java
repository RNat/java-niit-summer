package niit.summer.complex;
import java.lang.Math;
import java.text.DecimalFormat;

public class ComplexNumber {
	
public static final ComplexNumber ZERO = new ComplexNumber(0,0);

private final double real;

private final double imaginary;

	public ComplexNumber(double real1, double imaginary1) {
		real = real1;
		imaginary = imaginary1;
	}

	public ComplexNumber add(ComplexNumber z2) {
		ComplexNumber result = new ComplexNumber(real + z2.real, imaginary + z2.imaginary);
		return result;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(imaginary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(real);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComplexNumber other = (ComplexNumber) obj;
		if (Double.doubleToLongBits(imaginary) != Double.doubleToLongBits(other.imaginary))
			return false;
		if (Double.doubleToLongBits(real) != Double.doubleToLongBits(other.real))
			return false;
		return true;
	}

	public ComplexNumber multiply(ComplexNumber z2) {
		ComplexNumber result = new ComplexNumber(this.real*z2.real - this.imaginary*z2.imaginary, this.imaginary*z2.real + this.real*z2.imaginary);
		return result;
	}

	public double abs() {
		return Math.sqrt(this.real*this.real + this.imaginary*this.imaginary);
	}

	public double arg() {
		Double result;
		if(this.equals(ZERO))
			result = Double.NaN;
		else if(this.real > 0)
			result = Math.atan(this.imaginary/this.real);
		else if(this.real == 0)
			result = Math.PI/2 * Math.signum(imaginary);
		else
			result = Math.PI * Math.signum(imaginary + 0.00000001) + Math.atan(this.imaginary/this.real);
		
		return result;
	}
	
	public AlgebraicRepresentation asAlgebraic() {
		return new AlgebraicRepresentation();
	}

        public String Formatting(double number, String templ)
        {
         DecimalFormat decimalFormat = new DecimalFormat(templ);
         return decimalFormat.format(number);
        }
	
	public class AlgebraicRepresentation 
	{
    public AlgebraicRepresentation(){}

	public double getReal() {
		return real;
	}

	public double getImaginary() {
		return imaginary;
	}
	
	public String toString()
	{
                String formattedR = Formatting(real, "###");
                String formattedIm = Formatting(imaginary, "###");
                String result;
		
	    if ((real == 0)&&(imaginary == 0))
			result = "0";
	    else if (real == 0)
			result = formattedIm + "i";
		else if (imaginary == 0)
			result = formattedR;
		else if (imaginary < 0)
			result = formattedR + formattedIm + "i";
		else
			result = formattedR + " + " + formattedIm + "i";
	    
	    return result;
	}
	}

	public TrigonometricRepresentation asTrigonometric() {

                if(this.equals(ZERO))
                    throw new IllegalArgumentException();
                else
		return new TrigonometricRepresentation();
	}
	
	public class TrigonometricRepresentation
	{
		public TrigonometricRepresentation(){}

		public double getAbsoluteValue() {
			return abs();
		}

		public double getArgument() {
			return arg();
		}
		
		public String toString()
		{
			String angle = Formatting(Math.toDegrees(getArgument()),"###");
			String absVal = Formatting(getAbsoluteValue(),"0.###");
			String result;
			
			if((real != 0)&&(imaginary != 0))
				result = absVal + "(cos" + angle + "°+i*sin" + angle + "°)";
			else if((imaginary == 0)&&(real != 0))
				result = absVal;
			else if(getAbsoluteValue() == 1)
				result = "i*sin" + angle + "°";
			else
				result = absVal + "*i*sin" + angle + "°";
			
			return result;
		}
		
	}
	
	}