package niit.summer.complex;

public class Utils {
    public static boolean isEqual(double a, double b) {
        return Double.compare(a, b) == 0;
    }
}
