package niit.summer.rover;

public interface Liftable {
    void lift();
}
