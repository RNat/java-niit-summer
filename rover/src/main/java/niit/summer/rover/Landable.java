package niit.summer.rover;

import niit.summer.rover.constants.Direction;

public interface Landable {
    void land(Point position, Direction direction);
}
