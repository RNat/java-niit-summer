package niit.summer.rover;

public interface Moveable {
    void move();
}
