package niit.summer.rover;

import niit.summer.rover.constants.CellState;
import niit.summer.rover.constants.Direction;

public class GroundVisor {
private Ground planet;

public GroundVisor(Ground planet)
{
	this.planet = planet;
}

public Ground getGround()
{
	return planet;
}

public boolean hasObstacles(Point position) throws OutOfGroundException
{
	Integer x = position.GetX();
	Integer y = position.GetY();
	if ((x < 0)||(y < 0)||(x > planet.GetLength())||(y > planet.GetWidth()))
		throw new OutOfGroundException();
	CellState state = planet.getCell(x, y).getState();
	return (CellState.OCCUPIED.equals(state));
}
}
