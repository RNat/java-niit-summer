package niit.summer.rover.programmable;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import niit.summer.rover.GroundVisor;
import niit.summer.rover.Rover;
import niit.summer.rover.command.RoverCommand;
import niit.summer.rover.stats.RoverStatsModule;
import niit.summer.rover.stats.SimpleRoverStatsModule;

/**
 * Этот класс должен уметь все то, что умеет обычный Rover, но при этом он еще должен уметь выполнять программы,
 * содержащиеся в файлах
 */
public class ProgrammableRover extends Rover implements ProgramFileAware{

	RoverStatsModule stats;
	private Map<String, Object> settings;
	
	public ProgrammableRover(GroundVisor groundVisor, SimpleRoverStatsModule simpleRoverStatsModule) {
		super(groundVisor);
		stats = simpleRoverStatsModule;
		settings = new HashMap<String, Object>();
	}

	public void executeProgramFile(String file) {
		RoverCommandParser parser = new RoverCommandParser(this, file);
		RoverProgram program = parser.getProgram();
		settings = program.getSettings();
		Collection<RoverCommand> commands = program.getCommands();
		for(RoverCommand nextCommand: commands)
		{
			if(settings.get(RoverProgram.STATS).equals(true))
				stats.registerPosition(getCurrentPosition());
			nextCommand.execute();
		}
	}

	public Map<String, Object> getSettings() {
		
		return Collections.unmodifiableMap(settings);
	}


}
