package niit.summer.rover.programmable;

import niit.summer.rover.Point;
import niit.summer.rover.command.LandCommand;
import niit.summer.rover.command.LiftCommand;
import niit.summer.rover.command.LoggingCommand;
import niit.summer.rover.command.MoveCommand;
import niit.summer.rover.command.RoverCommand;
import niit.summer.rover.command.TurnCommand;
import niit.summer.rover.constants.Direction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoverProgram {
    public static final String LOG = "log";
    public static final String STATS = "stats";
    public static final String SEPARATOR = "===";
    private Map<String, Object> settings;
    private List<RoverCommand> commands;
    
    public RoverProgram()
    {
    	settings = new HashMap<String, Object>();
    	commands = new ArrayList<RoverCommand>();
    }
    
	public Map<String, Object> getSettings() {
		return settings;
	}
    
	public Collection<RoverCommand> getCommands() {
		return commands;
	}
	
	public void setSettings(Map<String, Object> settings)
	{
		this.settings = settings;
	}
	
	public void setCommands(List<RoverCommand> commands)
	{
		this.commands = commands;

	}
}
