package niit.summer.rover.programmable;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

import niit.summer.rover.Point;
import niit.summer.rover.command.LandCommand;
import niit.summer.rover.command.LiftCommand;
import niit.summer.rover.command.LoggingCommand;
import niit.summer.rover.command.MoveCommand;
import niit.summer.rover.command.RoverCommand;
import niit.summer.rover.command.TurnCommand;
import niit.summer.rover.constants.Direction;


public class RoverCommandParser {

	private ProgrammableRover rover;
	private String file;
	
	public RoverCommandParser(ProgrammableRover rover, String file)
	{
		this.rover = rover;
		this.file = file;
	}
	
	public RoverProgram getProgram()  {

		List<RoverCommand> commands = new ArrayList<RoverCommand>();
		Map<String, Object> settings = new HashMap<String, Object>();
		RoverProgram program = new RoverProgram();
		BufferedReader reader = null;
		
		URL resource = this.getClass().getResource(file);
		if(resource == null)
			throw new RoverCommandParserException();
		
		try {
			
			reader = new BufferedReader(new FileReader(resource.getFile()));
			String nextLine;
			RoverCommand command = null;
			while((nextLine = reader.readLine()) != null)
			{
				String[] words = nextLine.split(" ");
				if (RoverProgram.LOG.equals(words[0]))
				{
					boolean value = words[1].equals("on");
					settings.put(words[0], value);
				}
				
				else if (RoverProgram.STATS.equals(words[0]))
				{
					boolean value = words[1].equals("on");
					settings.put(words[0], value);
				}
				
				else if ("move".equals(words[0]))
					command = new MoveCommand(rover);
				
				else if ("lift".equals(words[0]))
					command = new LiftCommand(rover);
				
				else if ("land".equals(words[0]))
					command = new LandCommand(rover, new Point(Integer.parseInt(words[1]), Integer.parseInt(words[2])), Direction.valueOf(words[3].toUpperCase()));
				
				else if ("turn".equals(words[0]))
					command = new TurnCommand(rover, Direction.valueOf(words[1].toUpperCase()));
				if(command != null)
				{
				if(settings.get(program.LOG).equals(true))
				   commands.add(new LoggingCommand(command));
				else
					commands.add(command);
				}
			}
			
		} catch (FileNotFoundException e1) {
			throw new RoverCommandParserException();
		}
		 catch (IOException e) {
			throw new RoverCommandParserException();
		}		
		finally 
		{
			try {
				reader.close();
			} catch (IOException e) {
				throw new RoverCommandParserException();
			}
			
		}
		program.setSettings(settings);
		program.setCommands(commands);
		return program;
	}


}
