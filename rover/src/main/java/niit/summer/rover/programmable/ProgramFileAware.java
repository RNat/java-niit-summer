package niit.summer.rover.programmable;

public interface ProgramFileAware {
    void executeProgramFile(String path);
}
