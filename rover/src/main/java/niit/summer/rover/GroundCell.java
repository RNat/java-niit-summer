package niit.summer.rover;

import niit.summer.rover.constants.CellState;

public class GroundCell {
	
	private CellState state;
	
	public GroundCell(CellState cellState)
	{
		this.state = cellState;
	}

	public CellState getState() {
		return this.state;
	}

}
