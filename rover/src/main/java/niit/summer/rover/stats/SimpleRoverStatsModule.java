package niit.summer.rover.stats;

import java.util.Collection;
import java.util.HashSet;

import niit.summer.rover.Point;

public class SimpleRoverStatsModule implements RoverStatsModule {

	private Collection<Point> stats;
	
	public SimpleRoverStatsModule()
	{
		stats = new HashSet<Point>();
	}
	
	@Override
	public void registerPosition(Point position) {
		stats.add(position);
	}

	@Override
	public boolean isVisited(Point point) {
        if(stats.contains(point)) return true;
        else return false;
	}

	@Override
	public Collection<Point> getVisitedPoints() {
		return stats;
	}

}
