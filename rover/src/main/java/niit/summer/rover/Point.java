package niit.summer.rover;

public class Point {
	private Integer x;
	private Integer y;
	public Point()
	{
		this.x = null;
		this.y = null;
	}
	
	public Point(int i, int j) {
		SetLocation(i, j);
	}

	public void SetLocation(Integer new_x, Integer new_y)
	{
		this.x = new_x;
		this.y = new_y;
	}
	
	public Integer GetX()
	{
		return x;
	}
	
	public Integer GetY()
	{
		return y;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((x == null) ? 0 : x.hashCode());
		result = prime * result + ((y == null) ? 0 : y.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (x == null) {
			if (other.x != null)
				return false;
		} else if (!x.equals(other.x))
			return false;
		if (y == null) {
			if (other.y != null)
				return false;
		} else if (!y.equals(other.y))
			return false;
		return true;
	}
	
	
    //не забудьте реализовать equals, hashCode, toString!
}
