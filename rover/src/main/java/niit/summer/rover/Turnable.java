package niit.summer.rover;

import niit.summer.rover.constants.Direction;

public interface Turnable {
    void turnTo(Direction direction);
}
