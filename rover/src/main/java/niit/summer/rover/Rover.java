package niit.summer.rover;

import niit.summer.rover.constants.Direction;

public class Rover implements Landable, Liftable, Moveable, Turnable
{
	private Direction facing;
	private Point RoverLocation;
	private GroundVisor visor;
	private boolean airborne;
	
	public Rover(GroundVisor visor)
	{
		RoverLocation = new Point(0,0);
		facing = Direction.SOUTH;
		this.visor = visor;
		airborne = false;
	}
	
	@Override
	public void turnTo(Direction direction) {
		this.facing = direction;	
	}

	@Override
	public void move() {
		if (!airborne) {
			Integer x = RoverLocation.GetX();
			Integer y = RoverLocation.GetY();
			if (facing == Direction.NORTH) {
				y--;
			} else if (facing == Direction.SOUTH) {
				y++;
			} else if (facing == Direction.EAST) {
				x++;
			} else {
				x--;
			}
			try {
				if(!visor.hasObstacles(new Point(x, y)))
				RoverLocation.SetLocation(x, y);
			} catch (OutOfGroundException e) {
				lift();
			}
		}
	}

	@Override
	public void lift() {
		airborne = true;
		RoverLocation = null;
		facing = null;
	}

	@Override
	public void land(Point position, Direction direction) {
		try {
			if(!visor.hasObstacles(position))
			{
			RoverLocation = position;
			facing = direction;	
			airborne = false;
			}
		} catch (OutOfGroundException e) {
			this.lift();
		}
	}

	public Point getCurrentPosition() {
		return RoverLocation;
	}

	public boolean isAirborne() {
		return airborne;
	}

	public Direction getDirection() {
		return facing;
	}
	
}
