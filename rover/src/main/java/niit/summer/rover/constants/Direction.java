package niit.summer.rover.constants;

public enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST
}
