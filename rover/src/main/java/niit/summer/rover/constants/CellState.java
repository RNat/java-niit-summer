package niit.summer.rover.constants;

public enum CellState {
    FREE, //свободная ячейка
    OCCUPIED //занятая ячейка
}
