package niit.summer.rover;

public class Ground {

private GroundCell[][] landscape;
private  int length, width;

	public Ground(int length, int width) {
		landscape = new GroundCell[length][width];
		this.length = length;
		this.width = width;
	}

	public void initialize(GroundCell...groundCells) {
	
		int i = 0, j = 0, capacity = length*width;
		
		if (capacity > groundCells.length) throw new IllegalArgumentException();
		for(int c = 0; c < capacity; c++)
		{
			landscape[i][j] = groundCells[c];
			
			if(i < length-1)
				i++;
			else
			{
				i = 0;
				j++;
			}

		}
		
	}

	public GroundCell getCell(int i, int j) throws OutOfGroundException{
		if((i < 0)||(j < 0)||(i >= length)||(j >= width))
			throw new OutOfGroundException();
		else return landscape[i][j];
	}

	public Integer GetLength()
	{
		return length;
	}
	
	public Integer GetWidth()
	{
		return width;
	}

}
