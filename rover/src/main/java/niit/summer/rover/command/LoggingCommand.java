package niit.summer.rover.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingCommand implements RoverCommand {
    private RoverCommand command;
	private static final Logger LOGGER = LoggerFactory.getLogger(LoggingCommand.class);

    public LoggingCommand(RoverCommand command) {
		this.command = command;
	}
    
    public void execute()
    {
    	LOGGER.debug(command.toString());
    	command.execute();
    }

	@Override
	public String toString() {
		return command.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((command == null) ? 0 : command.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoggingCommand other = (LoggingCommand) obj;
		if (command == null) {
			if (other.command != null)
				return false;
		} else if (!command.equals(other.command))
			return false;
		return true;
	}
    

}
