package niit.summer.rover.command;

public interface RoverCommand {
    void execute();
}
