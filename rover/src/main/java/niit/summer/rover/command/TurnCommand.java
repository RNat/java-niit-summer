package niit.summer.rover.command;

import niit.summer.rover.Rover;
import niit.summer.rover.constants.Direction;

public class TurnCommand implements RoverCommand {

	private Rover rover;
	private Direction direction;
	


	public TurnCommand(Rover rover2, Direction direction2) {
		this.rover = rover2;
		this.direction = direction2;
	}

	@Override
	public void execute() {
		rover.turnTo(direction);
	}

	@Override
	public String toString() {
		return "Heading " + direction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direction == null) ? 0 : direction.hashCode());
		result = prime * result + ((rover == null) ? 0 : rover.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TurnCommand other = (TurnCommand) obj;
		if (direction != other.direction)
			return false;
		if (rover == null) {
			if (other.rover != null)
				return false;
		} else if (!rover.equals(other.rover))
			return false;
		return true;
	}

}
