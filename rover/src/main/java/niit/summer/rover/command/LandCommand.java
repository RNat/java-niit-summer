package niit.summer.rover.command;

import niit.summer.rover.Point;
import niit.summer.rover.Rover;
import niit.summer.rover.constants.Direction;

public class LandCommand implements RoverCommand {
	
	private Rover rover;
	private Point position;
	private Direction direction;

	public LandCommand(Rover rover, Point position, Direction direction) {
		this.rover = rover;
		this.position = position;
		this.direction = direction;
	}

	@Override
	public void execute() {
		rover.land(position, direction);
	}

	@Override
	public String toString() {
		return "Land at (" + position.GetX() + ", " + position.GetY() + ") heading " + direction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direction == null) ? 0 : direction.hashCode());
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		result = prime * result + ((rover == null) ? 0 : rover.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LandCommand other = (LandCommand) obj;
		if (direction != other.direction)
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (rover == null) {
			if (other.rover != null)
				return false;
		} else if (!rover.equals(other.rover))
			return false;
		return true;
	}

}
