package niit.summer.rover.command;

import niit.summer.rover.Rover;

public class MoveCommand implements RoverCommand {

	private Rover rover;
	
	public MoveCommand(Rover rover) {
		this.rover = rover;
	}

	@Override
	public void execute() {
		rover.move();
		
	}

	@Override
	public String toString() {
		return "Rover moved";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rover == null) ? 0 : rover.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MoveCommand other = (MoveCommand) obj;
		if (rover == null) {
			if (other.rover != null)
				return false;
		} else if (!rover.equals(other.rover))
			return false;
		return true;
	}

}
