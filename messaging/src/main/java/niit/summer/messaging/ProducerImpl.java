package niit.summer.messaging;

import java.util.Random;

public class ProducerImpl implements Runnable, Producer {

	public ProducerImpl()
	{
		
	}
	
	@Override
	public void produce() {
		Random random = new Random();
		Message currentMessage = new Message(random.nextInt(100));
		try {
			Thread.sleep(5);
			MessageBusImpl.getInstance().put(currentMessage);
		} catch (InterruptedException e) {
	     System.out.println("Empty message!");
		}
	}

	@Override
	public void run() {
		while(true)
			produce();
	}

}
