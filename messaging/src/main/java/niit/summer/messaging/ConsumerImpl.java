package niit.summer.messaging;

public class ConsumerImpl implements Runnable, Consumer {

	private int stopNumber;
	Message lastMessage;
	
	public ConsumerImpl(int stopNumber) {
		this.stopNumber = stopNumber;
		lastMessage = new Message();
	}

	@Override
	public boolean consume() {

			try {
				lastMessage = MessageBusImpl.getInstance().take();
			} catch (InterruptedException e) {
				System.out.println("Empty message!");
			}

		if (lastMessage.getPayload() == stopNumber)
			return false;
		else return true;
	}

	@Override
	public void run() {
		while(true)
		{
			if (!consume())
				break;
		}
       System.out.println("Consumer got message " + lastMessage.getPayload() + " and stopped working. MessageBus transferred "+ MessageBusImpl.getInstance().getTransferred() +" messages");
	}

}
