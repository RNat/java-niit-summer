package niit.summer.messaging;

import java.util.concurrent.ArrayBlockingQueue;

public class MessageBusImpl implements MessageBus {
	
	private ArrayBlockingQueue<Message> MessageQueue;
	private int count;
	
	private MessageBusImpl() 
	{
		MessageQueue = new ArrayBlockingQueue<Message>(10);
		count = 0;
	};
	
	private static class MessageSingleton
	{
		private static final MessageBusImpl Instance = new MessageBusImpl();
	}
	
	public synchronized static MessageBusImpl getInstance()
	{
		return MessageSingleton.Instance;
	}

	@Override
	public void put(Message message) throws InterruptedException {
		if (MessageQueue.size() < 10)
		    MessageQueue.put(message);
		
	}

	@Override
	public Message take() throws InterruptedException {
		
			count++;
			return MessageQueue.take();
		
	}

	@Override
	public synchronized int queueSize() {		
		return MessageQueue.size();
	}

	@Override
	public int getTransferred() {
		return count;
	}

}
